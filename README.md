# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
((UKAZI GIT))
```
git clone https://bp3201@bitbucket.org/bp3201/stroboskop.git

Naloga 6.2.3:
((UVELJAVITEV))
https://bitbucket.org/bp3201/stroboskop/commits/4a808be06f3d51bc4902032c26e21762dc178f68


## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
((UVELJAVITEV))
https://bitbucket.org/bp3201/stroboskop/commits/c32963575cf2072a09d0db94d842e7f85cdf73d2

Naloga 6.3.2:
((UVELJAVITEV))
https://bitbucket.org/bp3201/stroboskop/commits/684c2719da8361eb651e35f865dc3706e1a945af

Naloga 6.3.3:
((UVELJAVITEV))
https://bitbucket.org/bp3201/stroboskop/commits/f9e75cb0f0c7e39c217acc14c95ce39a4a4d6fb1

Naloga 6.3.4:
((UVELJAVITEV))
https://bitbucket.org/bp3201/stroboskop/commits/05d0a942ee8dcf1551af63e9ebf5a2b186e1b437

Naloga 6.3.5:

```
((UKAZI GIT))
```
git checkout master
git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
((UVELJAVITEV))
https://bitbucket.org/bp3201/stroboskop/commits/4e1c7deaf3e352c0617d31b8ac7d4f9926c64d3b

Naloga 6.4.2:
((UVELJAVITEV))
https://bitbucket.org/bp3201/stroboskop/commits/81fa4d68e54d3f00112c284ca72acf7eca93dc76

Naloga 6.4.3:
((UVELJAVITEV))
https://bitbucket.org/bp3201/stroboskop/commits/e7046e74047b064e84d32ef4fe140ab420c1a32f

Naloga 6.4.4:
((UVELJAVITEV))
https://bitbucket.org/bp3201/stroboskop/commits/ee789e195ce9e73bdf36e09561e6fb80443c0e6a